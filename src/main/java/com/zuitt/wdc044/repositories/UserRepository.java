package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Object> {

    //a custom method for finding a user by their username
    // this will also be used in creating a web token (JWT Token)
    //Spring Data (Query Creation)
        //The mechanism strips the prefixes findBy from the method and starts parsing the rest of it

    User findByUsername(String username);



}
