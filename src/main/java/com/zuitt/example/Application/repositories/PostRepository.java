package com.zuitt.example.Application.repositories;

import com.zuitt.example.Application.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

}